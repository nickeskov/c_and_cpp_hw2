cmake_minimum_required(VERSION 3.6)

project(hw2)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(GCC_COVERAGE_COMPILE_FLAGS "--coverage -fprofile-arcs -ftest-coverage -fPIC")
set(GCC_COVERAGE_LINK_FLAGS "-lgcov --coverage")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS} \
                        ${GCC_COVERAGE_LINK_FLAGS} \
                        -std=c++11 -Wall -Wextra -Werror -ggdb3")

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS} \
                    ${GCC_COVERAGE_LINK_FLAGS} \
                    -O0 -std=c11 -Wall -Wextra -Werror -ggdb3")

# ------------------------------------------------------------------------------
# Includes
# ------------------------------------------------------------------------------

include_directories(include)

# ------------------------------------------------------------------------------

if(ENABLE_CPPCHECK)

    list(APPEND CPPCHECK_ARGS
        --enable=warning,style,performance,portability,unusedFunction
        --std=c++11
        --verbose
        --error-exitcode=1
        --language=c++
        -I ${CMAKE_SOURCE_DIR}/include
        ${CMAKE_SOURCE_DIR}/include/*.h*
        ${CMAKE_SOURCE_DIR}/src/*.c*
        ${CMAKE_SOURCE_DIR}/test/*.c*
    )

    add_custom_target(
        check
        COMMENT "running cppcheck"
        COMMAND cppcheck ${CPPCHECK_ARGS}
    )

endif()

# find_package(GTest REQUIRED)
# add_executable(run_gtests test/tests.cpp src/hw1.c)
# target_link_libraries(run_gtests ${GTEST_LIBRARIES} pthread)

if(ENABLE_MULTITHREAD)
    add_library(elementary_checksum_multithread SHARED src/elementary_checksum.c)

    target_compile_definitions(elementary_checksum_multithread
                                PUBLIC ENABLE_MULTITHREAD MULTITHREAD_MODE_TEST)

    target_link_libraries(elementary_checksum_multithread pthread)
    add_executable(mytests_multithread test/test.c test/test_helpers.c)
    target_link_libraries(mytests_multithread elementary_checksum_multithread)

    add_executable(mytest_perfomance_multithread test/test_perfomance.c
                                                    test/test_helpers.c)

    target_link_libraries(mytest_perfomance_multithread
                                elementary_checksum_multithread)
else()
    add_library(elementary_checksum_singlethread STATIC src/elementary_checksum.c)

    add_executable(mytests_singlethread test/test.c test/test_helpers.c)
    target_link_libraries(mytests_singlethread elementary_checksum_singlethread)

    add_executable(mytest_perfomance_singlethread test/test_perfomance.c
                                                    test/test_helpers.c)
    target_link_libraries(mytest_perfomance_singlethread
                                elementary_checksum_singlethread)
endif()
