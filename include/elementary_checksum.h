#ifndef ELEMENTARY_CHECKSUM_H
# define ELEMENTARY_CHECKSUM_H

# include <stdbool.h>

bool    calc_checksum(const long *begin, const long *end, long *sum,
                      long modulo);

#endif /* ELEMENTARY_CHECKSUM_H */
