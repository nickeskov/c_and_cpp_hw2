#ifndef MYPREDEF_H
#define MYPREDEF_H

# define unlikely(expr)  (__builtin_expect(!!(expr), 0))
# define likely(expr)    (__builtin_expect(!!(expr), 1))

#endif /* MYPREDEF_H */
