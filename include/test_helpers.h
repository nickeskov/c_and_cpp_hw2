#ifndef TEST_HELPERS_H
# define TEST_HELPERS_H

# include <stdbool.h>
# include <string.h>

# define MODULO_COEF (1024)
# define mb_to_bytes(mbytes) ((mbytes) * 1024 * 1024)

long fill_arr(long *arr, size_t n, long modulo);

bool test_checksum(long *arr, size_t n, long modulo);

#endif
