#!/bin/sh
# execute after run_tests.sh
gcovr -r . build/
gcovr -r . build/ --html --html-details -o code_coverage_report.html
mkdir coverage
mv code_coverage_* coverage/
