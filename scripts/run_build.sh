#!/bin/sh
mkdir -p build && cd build
cmake .. -DENABLE_MULTITHREAD=OFF
make
cmake .. -DENABLE_MULTITHREAD=ON
make
