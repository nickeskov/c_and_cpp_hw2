#!/bin/sh
mkdir -p build && cd build

cmake .. -DENABLE_MULTITHREAD=OFF
make
valgrind --tool=memcheck --leak-check=full --log-file=../memcheck_singleth.log \
    ./mytests_singlethread $RANDOM

cmake .. -DENABLE_MULTITHREAD=ON
make
valgrind --tool=memcheck --leak-check=full --log-file=../memcheck_multith.log \
    ./mytests_multithread $RANDOM
