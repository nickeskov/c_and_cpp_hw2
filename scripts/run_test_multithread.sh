#!/bin/sh
mkdir -p build && cd build
cmake .. -DENABLE_MULTITHREAD=ON
make mytests_multithread
./mytests_multithread $RANDOM
