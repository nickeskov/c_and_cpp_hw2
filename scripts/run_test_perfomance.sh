#!/bin/sh
mkdir -p build && cd build

cmake .. -DENABLE_MULTITHREAD=OFF
make
SINGLE_LOG=$(./mytest_perfomance_singlethread 100)

cmake .. -DENABLE_MULTITHREAD=ON
make
MULTI_LOG=$(./mytest_perfomance_multithread 100)

echo "SINGLETHREAD:" $SINGLE_LOG >> ../perfomance_test.log
echo "MULTITHREAD:" $MULTI_LOG >> ../perfomance_test.log
