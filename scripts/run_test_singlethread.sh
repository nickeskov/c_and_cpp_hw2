#!/bin/sh
mkdir -p build && cd build
cmake .. -DENABLE_MULTITHREAD=OFF
make mytests_singlethread
./mytests_singlethread $RANDOM
