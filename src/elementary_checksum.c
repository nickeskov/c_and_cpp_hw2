#include "elementary_checksum.h"
#include "mypredef.h"

static inline long calc_sum(const long *begin, const long *end,
                             long modulo);

#ifndef ENABLE_MULTITHREAD

bool calc_checksum(const long *begin, const long *end, long *sum,
                      long modulo)
{
    if (unlikely(!begin || !end || !sum || modulo == 0 || begin >= end)
            || (begin <= sum && sum < end))
        return false;

    *sum = calc_sum(begin, end, modulo);
    return true;
}

#else

# ifdef MACOS
#  include <sys/param.h>
#  include <sys/sysctl.h>
# else
#  include <unistd.h>
# endif

# include <stdlib.h>
# include <pthread.h>
# include <string.h> /* for size_t */


typedef struct thread_data
{
    const long  *begin;
    const long  *end;
    long        sum;
    long        modulo;
}               thread_data_t;


static inline int   get_cores_num();

static void         *thread_calc_sum(void *thread_data);


bool    calc_checksum(const long *begin, const long *end, long *sum,
                      long modulo)
{
    if (unlikely(!begin || !end || !sum || modulo == 0 || begin >= end
            || (begin <= sum && sum < end)))
        return false;

    const int th_count = get_cores_num() - 1;

    if (unlikely(th_count  == 0))
    {
        *sum = calc_sum(begin, end, modulo);
        return (true);
    }

    pthread_t *pthreads_ids = (pthread_t *) malloc(
            sizeof(pthread_t) * th_count);
    thread_data_t *threads_data = (thread_data_t *) malloc(
            sizeof(thread_data_t) * th_count);

    if (unlikely(!pthreads_ids || !threads_data))
    {
        free(pthreads_ids);
        free(threads_data);
        return false;
    }

    const size_t elems_per_th = (end - begin) /
            (sizeof(long) * (th_count + 1));

    for (int i = 0; i < th_count; ++i)
    {
        threads_data[i].begin = begin + elems_per_th * i;
        threads_data[i].end = begin + elems_per_th * (i + 1);
        threads_data[i].sum = 0;
        threads_data[i].modulo = modulo;
        if (unlikely(pthread_create(pthreads_ids + i,
                NULL, thread_calc_sum, threads_data + i)))
        {
            for (int j = i - 1; j >= 0; --j)
                pthread_cancel(*(pthreads_ids + j));
            free(pthreads_ids);
            free(threads_data);
            return false;
        }
    }

    long main_sum = calc_sum(begin + th_count * elems_per_th, end, modulo);

    for (int i = 0; i < th_count; ++i)
    {
        if (unlikely(pthread_join(pthreads_ids[i], NULL)))
        {
            for (int j = th_count - 1; j >= 0; --j)
                pthread_cancel(*(pthreads_ids + j));
            free(pthreads_ids);
            free(threads_data);
            return false;
        }
        main_sum += threads_data[i].sum;
        main_sum %= modulo;
    }
    *sum = main_sum;

    free(threads_data);
    free(pthreads_ids);
    return true;
}


static void         *thread_calc_sum(void *thread_data)
{
    thread_data_t *th_data = (thread_data_t *) thread_data;

    th_data->sum = calc_sum(th_data->begin, th_data->end, th_data->modulo);

    pthread_exit(NULL);
}

static inline int get_cores_num()
{
# ifdef MULTITHREAD_MODE_TEST
    return 2;
# elif MACOS
    int     nm[2];
    size_t  len = 4;
    int     count;

    nm[0] = CTL_HW; nm[1] = HW_AVAILCPU;
    sysctl(nm, 2, &count, &len, NULL, 0);

    if (count < 1)
    {
        nm[1] = HW_NCPU;
        sysctl(nm, 2, &count, &len, NULL, 0);
        if (count < 1)
            count = 1;
    }
    return count;
# else
    return (int) sysconf(_SC_NPROCESSORS_ONLN);
# endif
}

#endif

static inline long calc_sum(const long *begin, const long *end,
                            long modulo)
{
    long     result_sum = 0;

    while (begin < end)
    {
        result_sum += *begin % modulo;
        result_sum %= modulo;
        ++begin;
    }
    return result_sum;
}
