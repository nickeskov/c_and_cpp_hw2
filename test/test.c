/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ecathryn <ecathryn@studen.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/26 13:59:49 by ecathryn          #+#    #+#             */
/*   Updated: 2019/10/26 14:03:57 by ecathryn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include "test_helpers.h"
#include "elementary_checksum.h"

#define MODULO_COEF (1024)
#define TEST_SIZE (4096)
#define TEST_MBSIZE (mb_to_bytes(10))

#define TEST_ITERS (2)

static bool    test_incorrect_input(long *arr, int n)
{
    long value;

    return
        !(  calc_checksum(NULL, NULL, NULL, 0)
        ||  calc_checksum(arr, arr, &value, MODULO_COEF)
        ||  calc_checksum(arr, arr + n, arr + n / 2, MODULO_COEF)
        ||  calc_checksum(arr, arr + n, arr + n - 1, MODULO_COEF)
        ||  calc_checksum(NULL, arr + n, &value, MODULO_COEF)
        ||  calc_checksum(arr, NULL, &value, MODULO_COEF)
        ||  calc_checksum(arr, arr + n, NULL, MODULO_COEF)
        ||  calc_checksum(arr, arr + n, &value, 0)
        );
}


int     main(int argc, char **argv)
{
    long small_arr[TEST_SIZE];
    const long small_len = TEST_SIZE / sizeof(long);

    if (test_incorrect_input(small_arr, small_len) == false)
    {
        puts("[FAIL] Test incorrect input\n");
        return 1;
    }

    if (argc == 2)
    {
        unsigned int seed = (unsigned int) atoi(argv[1]);
        srand(seed);
    }

    long *big_arr = (long *) malloc(TEST_MBSIZE);
    const size_t big_len = TEST_MBSIZE / sizeof(long);
    if (!big_arr)
    {
        printf("%s\n", strerror(errno));
        return errno;
    }

    for (size_t i = 0; i < TEST_ITERS; ++i)
        if (test_checksum(small_arr, small_len, MODULO_COEF) == false
            || test_checksum(big_arr, big_len, MODULO_COEF) == false)
        {
            printf("[FAIL %zu] Failed main test on %zu iteration\n", i, i);
        }
    free(big_arr);
    return 0;
}
