#include "test_helpers.h"
#include "elementary_checksum.h"

#include <stdlib.h>

long fill_arr(long *arr, size_t n, long modulo)
{
    long res = 0;

    for (size_t i = 0; i < n; ++i)
    {
        arr[i] = rand();
        res += arr[i] % modulo;
        res %= modulo;
    }
    return res;
}

bool test_checksum(long *arr, size_t n, long modulo)
{
    const long sum = fill_arr(arr, n, modulo);
    long calc_sum_val = modulo + 1;

    if (calc_checksum(arr, arr + n, &calc_sum_val, modulo))
        return calc_sum_val == sum;

    return false;
}
