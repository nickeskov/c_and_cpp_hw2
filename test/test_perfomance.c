#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include "test_helpers.h"
#include "elementary_checksum.h"

typedef unsigned long long  t_ullolg;

static inline t_ullolg  rdtsc(void)
{
    unsigned int lo;
    unsigned int hi;

    __asm__ __volatile__ ( "rdtsc" : "=a" (lo), "=d" (hi) );
    return (((t_ullolg)hi << 32) | lo);
}


int     main(int argc, char **argv)
{
    size_t stress_test_mb = 100;

    if (argc == 2)
        stress_test_mb = (unsigned int) atoi(argv[1]);

    long *big_arr = (long *) malloc(mb_to_bytes(stress_test_mb));
    const size_t big_len = mb_to_bytes(stress_test_mb) / sizeof(long);
    if (!big_arr)
    {
        printf("%s\n", strerror(errno));
        return errno;
    }

    long tmp;
    clock_t time_begin = clock();
    t_ullolg tics_count = rdtsc();
    bool is_succes = calc_checksum(big_arr, big_arr + big_len,
                                    &tmp, MODULO_COEF);
    tics_count = rdtsc() - tics_count;
    unsigned int msec = ((unsigned int) (clock() - time_begin)) * 1000 /
                            CLOCKS_PER_SEC;

    if (is_succes == false)
    {
        free(big_arr);
        return 1;
    }

    printf("Time taken %u seconds, %u milliseconds (%llu processor ticks)\n",
        msec / 1000, msec % 1000, tics_count);

    free(big_arr);
    return 0;
}
